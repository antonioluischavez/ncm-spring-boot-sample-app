package com.neueda.ncmsampleapp;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

@RestController
@RequestScope
@RequestMapping("/api/v1/properties")
public class Controller {

    private String propertyA;

    private boolean propertyB;

    private Float propertyC;

    public Controller(@Value("${propertyA}") String propertyA,
                      @Value("${propertyB}") boolean propertyB,
                      @Value("${propertyC}") Float propertyC) {
        this.propertyA = propertyA;
        this.propertyB = propertyB;
        this.propertyC = propertyC;
    }

    @GetMapping("/a")
    public String getPropertyA() {
        return this.propertyA;
    }

    @GetMapping("/b")
    public boolean getPropertyB() {
        return this.propertyB;
    }

    @GetMapping("/c")
    public Float getPropertyC() {
        return this.propertyC;
    }

}
