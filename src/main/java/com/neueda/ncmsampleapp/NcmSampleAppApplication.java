package com.neueda.ncmsampleapp;

import com.neueda.ncm.client.spring_boot.config.EnableNeuedaConfigManager;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableNeuedaConfigManager
public class NcmSampleAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(NcmSampleAppApplication.class, args);
    }

}
